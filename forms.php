<?php include "header.php";?>
	<?php include "nav.php";?>
	<div class="container">
		<div class="row"> 
			<!-- start: Main Menu -->
			<?php include 'menu.php';?>
			<!-- end: Main Menu -->
			<!-- start: Content -->
			<div id="content" class="col-sm-11 padding0">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-primary">
						  	<div class="panel-heading">
						    	<h3 class="panel-title"><span class="glyphicons notes"><i></i></span>Files

						    	</h3>
						  	</div>
						  	<div class="panel-body">
						  		<table class="table table-striped">
							        <tbody><tr>
							            <td>
							                <b>Type of Form</b> 
							            </td>
							            <td>
							               <b>Description</b> 
							            </td>
							        </tr>
							        <tr>
							            <td><a href="#" target="_blank">SNHA - 01</a></td>
							            <td>
							                Member's Policy
							            </td>
							        </tr>
							        <tr>
							            <td><a href="#" target="_blank">SNHA - 02</a></td>
							            <td>
							                Member's Application Form
							            </td>
							        </tr>
							        <tr>
							            <td><a href="#" target="_blank">SNHA - 03</a></td>
							            <td>
							                Facility Rental Request Form
							            </td>
							        </tr>
							        <tr>
							            <td><a href="#" target="_blank">SNHA - 03</a></td>
							            <td>
							                Minutes of the Meeting
							            </td>
							        </tr>

    							</tbody>
    						</table>
						  		

                        	</div>
                        </div>
					</div><!--/col-->	
				</div>	
			</div>
			<!-- end: Content -->
			<!-- start: Widgets Area -->
		</div><!--/row-->
	</div><!--/container-->
	<div class="clearfix"></div>
	<!-- Modal -->
	<div class="modal fade" id="addStockModal" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog">	    
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Add Stock</h4>
	      </div>
	      <div class="modal-body">
	      	<form role="form">
			    <label>Date</label>
				<div class="input-group">
				  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				  <input type="text" class="form-control" placeholder="" value="<?php echo date('Y-m-d');?>">
				</div>
				<label>Supplier</label>
				<div class="input-group">
				  <span class="input-group-addon"><i class="fa fa-user"></i></span>
				  <input type="text" class="form-control" value="Alberto A. Mananay Jr.">
				</div>
				<br/>
				<label>Product Name</label>
				<div class="input-group">				  
					<span class="input-group-addon"><i class="fa fa-paste"></i></span>
				  <input type="text" class="form-control" value="Product 1">
				</div>
				<br>
				<label>Quantity</label>
				<div class="input-group">				  
				  <input type="text" class="form-control" value="">
				</div>
                <br>
                <label>Unity Type</label>
				<div class="input-group">				  
				  <input type="text" class="form-control" value="Kilogram" disabled>
				</div>
                <br>
			    <label>Total Amount</label>
				<div class="input-group">
				  <span class="input-group-addon"><i class="fa fa-money"></i>
				  </span>
				  <input type="text" class="form-control" placeholder="Php">
				</div>
			</form>	                                                     
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary" id="addCustomerBtn">Save</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
<?php include "footer.php";?>