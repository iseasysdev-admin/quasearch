<?php include "header.php";?>
<?php include "nav.php";?>
<div class="container">
    <div class="row">
        <!-- start: Content -->
        <!-- start: Content -->
        <div id="content" class="col-sm-12 padding0">
            <div class="form-member">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="glyphicons group"><i></i></span>Create New Survey</h3>
                            </div>
                            <div class="panel-body">
                                <!-- add member form-->
                                <h2>Survey Details</h2>
                                <form action="/" method="post">
                                    <section class="section institution-info">
                                        <div class="well">
                                            <div class="row">
                                                <div class="col-lg-12 form-group">
                                                    <div class="form-group">
                                                        <label for="survey-name">
                                                            Survey Name
                                                        </label>
                                                        <input type="text" class="col-lg-12 form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="survey-description">
                                                            Form Description
                                                        </label>
                                                        <textarea class="form-control" rows="3"></textarea>
                                                    </div>
                                                    <p>Do you want to include basic profile for your respondent <small>(e.g Name, Age,
                                                    Gender etc.)</small></p>
                                                    <div class="radio row">
                                                        <div class="col-md-6">
                                                            <label>
                                                                <input type="radio" value="yes"> Yes
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label>
                                                                <input type="radio" value="no"> No
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <!-- if yes -->
                                                    <p>Check all applicable data:</p>
                                                    <div class="listing">
                                                        <label class="checkbox">
                                                            <input type="checkbox" value="option1"> Name (Lastname, Firstname Middlename)
                                                        </label>
                                                        <label class="checkbox">
                                                            <input type="checkbox" value="option2"> Name (Lastname and Firstname only)
                                                        </label>
                                                        <label class="checkbox">
                                                            <input type="checkbox" value="option3"> Age
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <h2>Questions</h2>
                                    <section class="section questions">
                                        <div class="well">
                                            <div class="row question">
                                                <div class="col-lg-12 form-group">
                                                    <div class="form-group">
                                                        <label for="survey-name">
                                                            Question Title
                                                        </label>
                                                        <input type="text" class="col-lg-12 form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="survey-name">
                                                            Question Type
                                                        </label>
                                                        <select class="form-control">
                                                            <option>Text</option>
                                                            <option>Paragraph TExt</option>
                                                            <option>Multiple Choice</option>
                                                            <option>Checkboxes</option>
                                                            <option>Choose from a List</option>
                                                            <option>Date</option>
                                                            <option>Time</option>
                                                        </select>
                                                        <!-- if Text is selected -->
                                                        <input type="text" class="col-lg-12 form-control">
                                                        <!-- if Paragraph Text is selected -->
                                                        <textarea class="form-control" rows="3"></textarea>
                                                        <!-- if Multiple Choice is selected -->
                                                        <div class="col-d-12 listing-input">
                                                            <div class="row">
                                                                <div class="col-md-1 col-sm-1 col-xs-1">
                                                                    <label>
                                                                        <input type="radio" value="yes">
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-11 col-sm-11 col-xs-11">
                                                                    <input type="text" class="col-lg-12 form-control">
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-1 col-sm-1 col-xs-1">
                                                                    <label>
                                                                        <input type="radio" value="yes">
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-11 col-sm-11 col-xs-11">
                                                                    <input type="text" class="col-lg-12 form-control">
                                                                </div>
                                                            </div>
                                                            <button class="btn btn-default btn-md">Add Another Option</button>
                                                        </div>
                                                        <!-- if Checkboxes is selected -->
                                                        <div class="col-d-12 listing-input">
                                                            <div class="row">
                                                                <div class="col-md-1 col-sm-1 col-xs-1">
                                                                    <label>
                                                                        <input type="checkbox" value="yes">
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-11 col-sm-11 col-xs-11">
                                                                    <input type="text" class="col-lg-12 form-control">
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-1 col-sm-1 col-xs-1">
                                                                    <label>
                                                                        <input type="checkbox" value="yes">
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-11 col-sm-11 col-xs-11">
                                                                    <input type="text" class="col-lg-12 form-control">
                                                                </div>
                                                            </div>
                                                            <button class="btn btn-default btn-md">Add Another Option</button>
                                                        </div>
                                                        <!-- if Choose from a List is selected -->
                                                        <div class="col-d-12 listing-input">
                                                            <div class="row">
                                                                <div class="col-md-1 col-sm-1 col-xs-1">
                                                                    <label>
                                                                        1.
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-11 col-sm-11 col-xs-11">
                                                                    <input type="text" class="col-lg-12 form-control">
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-1 col-sm-1 col-xs-1">
                                                                    <label>
                                                                        2.
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-11 col-sm-11 col-xs-11">
                                                                    <input type="text" class="col-lg-12 form-control">
                                                                </div>
                                                            </div>
                                                            <button class="btn btn-default btn-md">Add Another Option</button>
                                                        </div>
                                                        <button class="btn btn-info btn-md">Done</button>
                                                        <hr>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <button class="btn btn-primary btn-md">Add Another Question</button>
                                        </div>
                                        <a href="preview-survey.php" class="btn btn-primary btn-lg">Submit Survey</a>
                                    </section>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<?php include "footer.php";?>