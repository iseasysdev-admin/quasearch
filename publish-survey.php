<?php include "header.php";?>
<?php include "nav.php";?>
<div class="container">
	<div class="row">
		<!-- start: Content -->
		<div id="content" class="col-md-12 padding0">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Dashboard</em>
					</h3>
				</div>
				<div class="panel-body">
					<!-- for admin and first time user start here-->
					<div class="row">
						<div class="col-md-6 col-md-offset-3"><a href="create-survey.php" class="btn btn-primary btn-lg">Create New Survey</a></div>
					</div>
					<!-- for admin and first time user end here-->
					<!-- for admin and second time user start here-->
					<div class="row">
						<div class="col-md-4 col-sm-12 col-xs-12"><a href="create-survey.php" class="btn btn-primary btn-lg">Create New Survey</a></div>
						<div class="col-md-4 col-sm-12 col-xs-12"><a href="manage-survey.php" class="btn btn-primary btn-lg">Manage Survey</a></div>
						<div class="col-md-4 col-sm-12 col-xs-12"><a href="publish-survey.php" class="btn btn-primary btn-lg">Published Survey</a></div>
					</div>
					<!-- for admin and second time user end here-->
				</div>
			</div>
		</div>
		<!-- end: Content -->
		<!-- start: Widgets Area -->
	</div><!--/row-->
</div><!--/container-->
<div class="clearfix"></div>
<?php include "footer.php";?>