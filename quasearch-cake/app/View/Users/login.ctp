<div class="login-box">	
	<div class="col-lg-12">		
		<h3><b><?php echo Configure::read('App.title');?></b> <small>Login</small></h3>
		<?php echo $this->Session->flash(); ?>
		<?php echo $this->Form->create('User',array('inputDefaults' => array('label' => false,'div' => false)));?>
			<div class="input-group">
				<span class="input-group-addon"><i class="icon-user"></i></span>				
				<?php echo $this->Form->input('username',array('class'=>'form-control','placeholder'=>'type username'));?>
			</div>
			<div class="clearfix"></div>
			<br>
			<div class="input-group">
				<span class="input-group-addon"><i class="icon-unlock"></i></span>			
				<?php echo $this->Form->input('password',array('class'=>'form-control','placeholder'=>'type password'));?>
			</div>
			<div class="clearfix"></div>
			<div class="button-login">	
				<?php echo $this->Form->submit('Login',array('class'=>'btn btn-primary'));?>
			</div>
			<div class="clearfix"></div>
			<br/>
		</form>
		</div>
	</div> 
</div>