<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AppHelper extends Helper {

	public $helpers = array('Html','Number','Time');

	function num_to_letter($num, $uppercase = FALSE){
		//$num -= 1;
		$letter = chr(($num % 26) + 97);
		$letter .= (floor($num/26) > 0) ? str_repeat($letter, floor($num/26)) : '';
		return ($uppercase ? strtoupper($letter) : $letter);
	}

	public function logInfo($log){
		$logInfo ='';

		$json_object = json_decode($log['Log']['json_object']);		
		$entityInfo = $log['Log']['model'];

		if($log['Log']['model']=='Homeowner'){
			$entityInfo .= " '{$json_object->Homeowner->full_name}'";
		}else if($log['Log']['model']=='Product'){
			$entityInfo .= " '{$json_object->Product->name}'";
		}else if($log['Log']['model']=='Purchase'){
			$entityInfo = "Purchase Order #{$json_object->Purchase->id}";
		}else{
			$entityInfo = $log['Log']['model'].' #'.$log['Log']['entity_id'];
		}

		//$entityInfo = $log['Log']['model'].' #'.$log['Log']['entity_id'];



		//$logInfo = $this->Html->link($entityInfo,array('controller'=>Inflector::pluralize(strtolower($log['Log']['model'])),'action'=>'view',$log['Log']['entity_id']));
		$logInfo = $entityInfo;

		


		if($log['Log']['event']=='CREATE'){
			$logInfo .= ' has been CREATED';			 
		}else if($log['Log']['event']=='EDIT'){
			$logInfo .= ' has been UPDATED';
		}else if($log['Log']['event']=='DELETE'){
			$logInfo .= ' has been DELETED';
		}

		if(!empty($log['Source']['username'])){
			$user = $this->Html->link($log['Source']['username'],array('controller'=>'users','action'=>'view',$log['Source']['id']));
			$logInfo .=' by '.$user;	
		}
		$logInfo .='.';
		return $logInfo;
	}


	public function status($num){
		$status = Configure::read('App.status');
		return $status[$num];
	}

	public function currency($amount){		
		return $this->Number->currency($amount,'Php ');
	}

	//Homeowner Photo Thumbnail
	public function avatar($homeowner){
		$noimage = '<img alt="100%x160" class="thumbnail" data-src="holder.js/100%x180" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNzEiIGhlaWdodD0iMTgwIj48cmVjdCB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgZmlsbD0iI2VlZSIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9Ijg1LjUiIHk9IjkwIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjEycHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+MTcxeDE4MDwvdGV4dD48L3N2Zz4=">';
		if(empty($homeowner['Homeowner']['photo_dir'])) return $noimage;
		
		$photo_dir = WWW_ROOT.'files/homeowner/photo/'.$homeowner['Homeowner']['photo_dir'].'/'.$homeowner['Homeowner']['photo'];
		if(!empty($homeowner['Homeowner']['photo']) && file_exists($photo_dir)){
			return $this->Html->image('/files/homeowner/photo/'.$homeowner['Homeowner']['photo_dir'].'/'.'thumb_'.$homeowner['Homeowner']['photo'],array('class'=>'thumbnail'));
		}else{
			return $noimage;
		}
	}

	//Homeowner Profile Link
	public function profileLink($homeowner,$type = 'homeowner'){

		if($type=='homeowner'){
			$full_name = @$homeowner['full_name'];
			if(empty($full_name)){
				$full_name = $homeowner['first_name'].' '.$homeowner['last_name'];
			}

			return $this->Html->link(ucwords($full_name), array('controller' => 'homeowners', 'action' => 'profile', $homeowner['id']),array('target'=>'_blank'));
		}else{
			return $this->Html->link(ucwords($homeowner['name']), array('controller' => 'consumers', 'action' => 'view', $homeowner['id']),array('target'=>'_blank'));
		}
		
	}
	//show period from - period to info
	public function period($data){
		return $this->date($data['period_from']).' to '.$this->date($data['period_to']);
	}


	/*
	 * General Formatting for showing dates
	 * return date
	 */
	function date($date){
		//return $this->Time->format('M jS, Y h:i a',$date);
		return $this->Time->format('d-m-Y',$date);
	}

	function invoiceNumber($invoice){
		if(!empty($invoice['created'])){			
			return $this->Time->format('dmY',$invoice['created']).'-'.$invoice['id'];	
		}else{
			return date('dmY');	
		}

		return false;
		
	}
}
