<!DOCTYPE html> 
<html> 
	<head> 
	<title><?php echo Configure::read('App.title');?> - <?php echo $title_for_layout;?></title>
  <link href="<?php echo $this->Html->url('/assets/css/bootstrap.min.css');?>" rel="stylesheet">
  <link href="<?php echo $this->Html->url('/assets/css/style.min.css');?>" rel="stylesheet">

  <link rel="stylesheet" href="http://code.jquery.com/mobile/1.0a2/jquery.mobile-1.0a2.min.css" /> 
  <script src="http://code.jquery.com/jquery-1.4.4.min.js"></script>  
  <script src="http://code.jquery.com/mobile/1.0a2/jquery.mobile-1.0a2.min.js"></script>

  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>


</head> 
<body> 

<!-- Page starts here -->
<div data-role="page" data-theme="b" id="page1">
  <?php echo $this->fetch('content'); ?>
</div> <!-- page1 -->


<!-- Page ends here -->
<!-- start: JavaScript-->   
  
  <script src="<?php echo $this->Html->url('/js/jquery.form.js');?>"></script>
  <script src="<?php echo $this->Html->url('/js/jquery.validate.js');?>"></script>    
  <script src="<?php echo $this->Html->url('/js/jquery.form.wizard.js');?>"></script> 
  <script src="<?php echo $this->Html->url('/js/jquery.cookie.js');?>"></script> 

</body>
</html>
