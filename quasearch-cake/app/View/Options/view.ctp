<div class="options view">
<h2><?php echo __('Option'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($option['Option']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($option['Option']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($option['Option']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($option['Option']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($option['Option']['status']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Option'), array('action' => 'edit', $option['Option']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Option'), array('action' => 'delete', $option['Option']['id']), null, __('Are you sure you want to delete # %s?', $option['Option']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Options'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Option'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Option Items'), array('controller' => 'option_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Option Item'), array('controller' => 'option_items', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Option Items'); ?></h3>
	<?php if (!empty($option['OptionItem'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Option Id'); ?></th>
		<th><?php echo __('Value'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($option['OptionItem'] as $optionItem): ?>
		<tr>
			<td><?php echo $optionItem['id']; ?></td>
			<td><?php echo $optionItem['option_id']; ?></td>
			<td><?php echo $optionItem['value']; ?></td>
			<td><?php echo $optionItem['created']; ?></td>
			<td><?php echo $optionItem['modified']; ?></td>
			<td><?php echo $optionItem['status']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'option_items', 'action' => 'view', $optionItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'option_items', 'action' => 'edit', $optionItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'option_items', 'action' => 'delete', $optionItem['id']), null, __('Are you sure you want to delete # %s?', $optionItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Option Item'), array('controller' => 'option_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
