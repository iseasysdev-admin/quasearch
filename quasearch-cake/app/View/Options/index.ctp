<div class="options index">
	<h2><?php echo __('Options'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th>Option Items</th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($options as $option): ?>
	<tr>
		<td><?php echo h($option['Option']['id']); ?>&nbsp;</td>
		<td><?php echo h($option['Option']['title']); ?>&nbsp;</td>
		<td>
			<?php if (!empty($option['OptionItem'])): ?>
			<table cellpadding = "0" cellspacing = "0" class="table table-bordered">				
				<tr>
				<?php foreach ($option['OptionItem'] as $optionItem): ?>
					<td><?php echo $optionItem['value']; ?></td>
				<?php endforeach; ?>
				</tr>
			</table>
			<?php endif;?>
		</td>
		<td><?php echo h($option['Option']['status']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $option['Option']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $option['Option']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $option['Option']['id']), null, __('Are you sure you want to delete # %s?', $option['Option']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<?php echo $this->element('paging');?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Option'), array('action' => 'add')); ?></li>
	</ul>
</div>
