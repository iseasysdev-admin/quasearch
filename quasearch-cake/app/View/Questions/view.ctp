<div class="questions view">
<h2><?php echo __('Question'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($question['Question']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Survey Form'); ?></dt>
		<dd>
			<?php echo $this->Html->link($question['SurveyForm']['title'], array('controller' => 'survey_forms', 'action' => 'view', $question['SurveyForm']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($question['Question']['type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question'); ?></dt>
		<dd>
			<?php echo h($question['Question']['question']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total Sequence'); ?></dt>
		<dd>
			<?php echo h($question['Question']['total_sequence']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Weight'); ?></dt>
		<dd>
			<?php echo h($question['Question']['weight']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($question['Question']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($question['Question']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($question['Question']['status']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Question'), array('action' => 'edit', $question['Question']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Question'), array('action' => 'delete', $question['Question']['id']), null, __('Are you sure you want to delete # %s?', $question['Question']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Survey Forms'), array('controller' => 'survey_forms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Survey Form'), array('controller' => 'survey_forms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Answers'), array('controller' => 'answers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Answer'), array('controller' => 'answers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Question Options'), array('controller' => 'question_options', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question Option'), array('controller' => 'question_options', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Submission Details'), array('controller' => 'submission_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Submission Detail'), array('controller' => 'submission_details', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Answers'); ?></h3>
	<?php if (!empty($question['Answer'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Question Id'); ?></th>
		<th><?php echo __('Answer'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Stat'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($question['Answer'] as $answer): ?>
		<tr>
			<td><?php echo $answer['id']; ?></td>
			<td><?php echo $answer['question_id']; ?></td>
			<td><?php echo $answer['answer']; ?></td>
			<td><?php echo $answer['created']; ?></td>
			<td><?php echo $answer['modified']; ?></td>
			<td><?php echo $answer['stat']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'answers', 'action' => 'view', $answer['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'answers', 'action' => 'edit', $answer['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'answers', 'action' => 'delete', $answer['id']), null, __('Are you sure you want to delete # %s?', $answer['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Answer'), array('controller' => 'answers', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Question Options'); ?></h3>
	<?php if (!empty($question['QuestionOption'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Question Id'); ?></th>
		<th><?php echo __('Sequence Id'); ?></th>
		<th><?php echo __('Options'); ?></th>
		<th><?php echo __('Weight'); ?></th>
		<th><?php echo __('Point'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($question['QuestionOption'] as $questionOption): ?>
		<tr>
			<td><?php echo $questionOption['id']; ?></td>
			<td><?php echo $questionOption['question_id']; ?></td>
			<td><?php echo $questionOption['sequence_id']; ?></td>
			<td><?php echo $questionOption['options']; ?></td>
			<td><?php echo $questionOption['weight']; ?></td>
			<td><?php echo $questionOption['point']; ?></td>
			<td><?php echo $questionOption['created']; ?></td>
			<td><?php echo $questionOption['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'question_options', 'action' => 'view', $questionOption['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'question_options', 'action' => 'edit', $questionOption['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'question_options', 'action' => 'delete', $questionOption['id']), null, __('Are you sure you want to delete # %s?', $questionOption['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Question Option'), array('controller' => 'question_options', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Submission Details'); ?></h3>
	<?php if (!empty($question['SubmissionDetail'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Question Id'); ?></th>
		<th><?php echo __('Submission Id'); ?></th>
		<th><?php echo __('Sequence Id'); ?></th>
		<th><?php echo __('Value'); ?></th>
		<th><?php echo __('Text'); ?></th>
		<th><?php echo __('Point'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($question['SubmissionDetail'] as $submissionDetail): ?>
		<tr>
			<td><?php echo $submissionDetail['id']; ?></td>
			<td><?php echo $submissionDetail['question_id']; ?></td>
			<td><?php echo $submissionDetail['submission_id']; ?></td>
			<td><?php echo $submissionDetail['sequence_id']; ?></td>
			<td><?php echo $submissionDetail['value']; ?></td>
			<td><?php echo $submissionDetail['text']; ?></td>
			<td><?php echo $submissionDetail['point']; ?></td>
			<td><?php echo $submissionDetail['created']; ?></td>
			<td><?php echo $submissionDetail['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'submission_details', 'action' => 'view', $submissionDetail['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'submission_details', 'action' => 'edit', $submissionDetail['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'submission_details', 'action' => 'delete', $submissionDetail['id']), null, __('Are you sure you want to delete # %s?', $submissionDetail['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Submission Detail'), array('controller' => 'submission_details', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
