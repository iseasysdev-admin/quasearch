<!-- start: Main Menu -->
<div id="sidebar-left" class="col-sm-1">
	<div class="nav-collapse sidebar-nav collapse navbar-collapse bs-navbar-collapse">
		<ul class="nav nav-tabs nav-stacked main-menu">
			<li><a href="<?php echo $this->Html->url('/');?>"><i class="fa fa-bar-chart-o"></i><span class="hidden-sm"> Dashboard</span></a></li>	
			<li><a href="<?php echo $this->Html->url(array('controller'=>'surveys','action'=>'index'));?>"><i class="fa fa-folder-open"></i><span class="hidden-sm"> Surveys</span></a></li>
			<li><a href="<?php echo $this->Html->url(array('controller'=>'survey_forms','action'=>'index'));?>"><i class="fa fa-folder-open"></i><span class="hidden-sm"> Survey Forms</span></a></li>
			<li><a href="<?php echo $this->Html->url(array('controller'=>'questions','action'=>'index'));?>"><i class="fa fa-folder-open"></i><span class="hidden-sm"> Questions</span></a></li>
			<li><a href="<?php echo $this->Html->url(array('controller'=>'question_items','action'=>'index'));?>"><i class="fa fa-folder-open"></i><span class="hidden-sm"> Question Items</span></a></li>
			<li><a href="<?php echo $this->Html->url(array('controller'=>'options','action'=>'index'));?>"><i class="fa fa-folder-open"></i><span class="hidden-sm"> Options</span></a></li>
			<li><a href="<?php echo $this->Html->url(array('controller'=>'submissions','action'=>'index'));?>"><i class="fa fa-folder-open"></i><span class="hidden-sm"> Submissions</span></a></li>
		</ul>
	</div>
</div>
<a id="main-menu-toggle" class="hidden-xs open"><i class="icon-reorder"></i></a>
<!-- end: Main Menu -->