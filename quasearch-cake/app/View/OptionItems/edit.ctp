<div class="optionItems form">
<?php echo $this->Form->create('OptionItem',array('class'=>'form-horizontal')); ?>
	<fieldset>
		<legend><?php echo __('Edit Option Item'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('option_id');
		echo $this->Form->input('value');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>