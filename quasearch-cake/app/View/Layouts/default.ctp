<!DOCTYPE html>
<html lang="en">
<head>	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title><?php echo Configure::read('App.title');?> - <?php echo $title_for_layout;?></title>
	<meta name="description" content="<?php echo Configure::read('App.description');?>">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	<!-- start: CSS -->
	<link href="<?php echo $this->Html->url('/assets/css/bootstrap.min.css');?>" rel="stylesheet">
	<link href="<?php echo $this->Html->url('/assets/css/style.min.css');?>" rel="stylesheet">

	<link href="<?php echo $this->Html->url('/assets/css/retina.min.css');?>" rel="stylesheet">
	<link href="<?php echo $this->Html->url('/css/custom.css');?>" rel="stylesheet">
	<!-- end: CSS -->

	<!-- end: Favicon and Touch Icons -->
	<link href="<?php echo $this->Html->url('/css/font-awesome.min.css');?>" rel="stylesheet">

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script src="assets/js/respond.min.js"></script>
		<link href="assets/css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="assets/css/ie9.css" rel="stylesheet">
	<![endif]--> 
	<link rel="shortcut icon" href="<?php echo $this->Html->url('/assets/ico/favicon.png');?>">
	<!-- end: Favicon and Touch Icons -->

	<!-- start: JavaScript-->
	<script src="<?php echo $this->Html->url('/assets/js/jquery-1.10.2.min.js');?>"></script>

	<script type="text/javascript">
		var base_url = '<?php echo $this->Html->url("/",true);?>';
		$(document).ready(function(){
			$("#flashMessage").attr('class','alert alert-info');
		})
	</script>
	<?php
	    echo $this->Html->css(array('jquery.loader'));
	    echo $this->Html->script(array('jquery.loader'));
	    echo $this->fetch('css');
	    echo $this->fetch('script');
	?>
</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container">
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".sidebar-nav.nav-collapse">
			      <span class="icon-bar"></span>
			      <span class="icon-bar"></span>
			      <span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.html"><span><?php echo Configure::read('App.title');?></span></a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right"> 
						<li>
							<a class="btn" href="<?php echo $this->Html->url(array('controller'=>'settings','action'=>'index'));?>">
								<i class="halflings-icon white wrench"></i>
							</a>
						</li>
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="index.html#">
								<i class="halflings-icon white user"></i> <?php echo $this->Session->read('Auth.User.username');?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo $this->Html->url('/myprofile');?>"><i class="halflings-icon white user"></i> Profile</a></li>
								<li><a href="login.html"><i class="halflings-icon white off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
	<div class="container">
		<div class="row">  
			<?php echo $this->element('menu');?>
			<!-- start: Content -->
			<div id="content" class="col-sm-11">
				<?php echo $this->Session->flash(); ?>
				<?php echo $this->fetch('content'); ?>
			</div>
			<!-- end: Content --> 				

		</div><!--/row--> 
		
	</div><!--/container-->
	<div class="clearfix"></div>	
	<?php echo $this->element('footer');?>
	
	<!-- start: JavaScript-->		
	<script src="<?php echo $this->Html->url('/assets/js/bootstrap.min.js');?>"></script>
	<script src="<?php echo $this->Html->url('/js/jquery.form.js');?>"></script>
	<script src="<?php echo $this->Html->url('/js/jquery.validate.js');?>"></script>	
	<script src="<?php echo $this->Html->url('/assets/js/jquery-ui-1.10.3.custom.min.js');?>"></script>		
	<script src="<?php echo $this->Html->url('/js/jquery.form.wizard.js');?>"></script>	
	
	<!-- page scripts --> 
	<script type="text/javascript">
		$(document).ready(function(){
			$('.index > table').attr('class','table table-bordered');
		})
	</script> 
	<!-- end: JavaScript-->
	
</body>
</html>