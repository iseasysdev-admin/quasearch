<!DOCTYPE html>
<html lang="en">
<head>
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title><?php echo Configure::read('App.title');?> - <?php echo $title_for_layout;?></title>
	<meta name="description" content="<?php echo Configure::read('App.description');?>">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link href="<?php echo $this->Html->url('/assets/css/bootstrap.min.css');?>" rel="stylesheet">
	<link href="<?php echo $this->Html->url('/assets/css/style.min.css');?>" rel="stylesheet">
	<link href="<?php echo $this->Html->url('/assets/css/retina.min.css');?>" rel="stylesheet">
	<!-- end: CSS -->

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script src="assets/js/respond.min.js"></script>
		<link href="assets/css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="assets/css/ie9.css" rel="stylesheet">
	<![endif]--> 
	<link rel="shortcut icon" href="<?php echo $this->Html->url('/assets/ico/favicon.png');?>">
	<!-- end: Favicon and Touch Icons -->
	
	<style type="text/css">body { background: url("<?php echo $this->Html->url('/assets/img/bg-login.jpg');?>") !important; }</style> 

	<!-- start: JavaScript-->
	<script src="<?php echo $this->Html->url('/assets/js/jquery-2.0.3.min.js');?>"></script>	
	<?php echo $this->Html->script('jquery.validate');?>
	<?php echo $this->Html->script('additional-methods');?>	
	<?php echo $this->Html->script('jquery.metadata');?>
	<script type="text/javascript">
		var base_url = '<?php echo $this->Html->url("/",true);?>';
	</script>
	<?php
	    echo $this->Html->css(array('jquery.loader'));
	    echo $this->Html->script(array('jquery.loader'));
	    echo $this->fetch('css');
	    echo $this->fetch('script');
	?>
		
</head>

<body>
	<div class="container">
		<div class="row">
			<?php echo $this->fetch('content'); ?> 
		</div>
		
	</div><!--/container-->	 
	<script src="assets/js/bootstrap.min.js"></script>
	
		
	<!-- theme scripts -->
	<script src="<?php echo $this->Html->url('/assets/js/custom.min.js');?>"></script>
	<script src="<?php echo $this->Html->url('/assets/js/core.min.js');?>"></script>
		
	<!-- end: JavaScript-->
	
</body>
</html>