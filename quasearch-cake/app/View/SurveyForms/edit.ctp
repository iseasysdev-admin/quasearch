<div class="surveyForms form">
<?php echo $this->Form->create('SurveyForm',array('class'=>'form-horizontal')); ?>
	<fieldset>
		<legend><?php echo __('Edit Survey Form'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('parent_id',array('options'=>$parentSurveyForms));
		echo $this->Form->input('survey_id');
		echo $this->Form->input('title');
		echo $this->Form->input('description');
		echo $this->Form->input('sequence_no');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>