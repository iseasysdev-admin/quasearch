<div class="surveyForms index">
	<h2><?php echo __('Survey Forms'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo $this->Paginator->sort('survey_id'); ?></th>
		<th><?php echo $this->Paginator->sort('parent_id'); ?></th>		
		<th><?php echo $this->Paginator->sort('title'); ?></th>
		<th><?php echo $this->Paginator->sort('Sequence No'); ?></th>
		<th><?php echo $this->Paginator->sort('No. Of Questions'); ?></th>
		<th><?php echo $this->Paginator->sort('created'); ?></th>
		<th><?php echo $this->Paginator->sort('modified'); ?></th>
		<th><?php echo $this->Paginator->sort('status'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($surveyForms as $surveyForm): ?>
	<tr>
		<td><?php echo h($surveyForm['SurveyForm']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($surveyForm['Survey']['title'], array('controller' => 'surveys', 'action' => 'view', $surveyForm['Survey']['id'])); ?>
		</td>
		<td>
			<?php echo $surveyForm['ParentSurveyForm']['title']; ?>
		</td>
		<td><?php echo h($surveyForm['SurveyForm']['title']); ?>&nbsp;</td>

		<td><?php echo h($surveyForm['SurveyForm']['sequence_no']); ?>&nbsp;</td>
		<td><?php echo h($surveyForm['SurveyForm']['question_count']); ?>&nbsp;</td>
		<td><?php echo h($surveyForm['SurveyForm']['created']); ?>&nbsp;</td>
		<td><?php echo h($surveyForm['SurveyForm']['modified']); ?>&nbsp;</td>
		<td><?php echo h($surveyForm['SurveyForm']['status']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $surveyForm['SurveyForm']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $surveyForm['SurveyForm']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $surveyForm['SurveyForm']['id']), null, __('Are you sure you want to delete # %s?', $surveyForm['SurveyForm']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<?php echo $this->element('paging');?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Survey Form'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Surveys'), array('controller' => 'surveys', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Survey'), array('controller' => 'surveys', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
