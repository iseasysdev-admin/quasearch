<style type="text/css">
	.error{
		color: red;
		font-weight: normal;
	}
</style>
<h1><?php echo $survey['Survey']['title'];?></h1>
<?php echo $this->Form->create('Survey', array('id' => 'SurveyForm','class'=>'form-horizontal')); ?> 
<?php echo $this->Form->hidden('survey_id',array('value'=>$survey['Survey']['id'],'name'=>'survey_id'));?>
<div id="fieldWrapper" class="col-lg-12">
	<ol>
	<?php foreach ($forms as $key => $form):?>
		<li class="step" id="<?php echo $form['SurveyForm']['id'];?>">
			<?php echo $form['SurveyForm']['title'];?>
			<?php if(!empty($form['forms'])):?>
				<ol >
					<?php foreach($form['forms'] as $form):?>
						<li type="square">
							<?php echo $form['SurveyForm']['title'];?>
							<ul style="list-style: none outside none;">
								<?php foreach ($form['Question'] as $num => $question):?>
									<li >
										<?php if($question['type']=='Short Answer'):?>
											<?php echo $this->Form->input('answer['.$question['id'].']',array('name'=>'answer['.$question['id'].']','label'=>$question['question'],'required'=>'required'));?>
											<div class="clearfix"></div>
										<?php elseif($question['type']=='Long Answer'):?>
											<?php echo $this->Form->input('answer['.$question['id'].']',array('name'=>'answer['.$question['id'].']','type'=>'textarea','label'=>$question['question'],'required'=>'required'));?>
											<div class="clearfix"></div>
										<?php elseif($question['type']=='Multiple Items'):?>
											<?php echo $question['question'];?>
											<div class="col-lg-offset-1 col-lg-10">
												<table class="table table-bordered">								
													<tr>
														<th>Items</th>
														<?php foreach ($question['Option']['OptionItem'] as $key => $item):?>
															<th><?php echo $item['value'];?></th>
														<?php endforeach;?>
													</tr>								
													<?php foreach ($question['QuestionItem'] as $key => $question_item):?>
														<tr>
															<th style="white-space:nowrap"><?php echo $question_item['title'];?></th>
															<?php foreach ($question['Option']['OptionItem'] as $key => $item):?>
																<?php if(count($question_item['ChildQuestionItem']) < 1):?>
																<th>
																	<?php echo $this->Form->input('answer['.$question['id'].']['.$question_item['id'].']['.$item['id'].']',array('name'=>'answer['.$question['id'].']['.$question_item['id'].']['.$item['id'].']','label'=>$question['question'],'required'=>'required','label'=>false,'div'=>false));?>
																</th>
																<?php endif;?>
															<?php endforeach;?>
														</tr>
														<?php foreach ($question_item['ChildQuestionItem'] as $key => $question_subitem):?>
															<tr>
																<th style="padding-left:30px;white-space:nowrap"><?php echo $question_subitem['title'];?></th>
																<?php foreach ($question['Option']['OptionItem'] as $key => $item):?>
																	<th>
																		<?php echo $this->Form->input('answer['.$question['id'].']['.$question_subitem['id'].']['.$item['id'].']',array('name'=>'answer['.$question['id'].']['.$question_subitem['id'].']['.$item['id'].']','label'=>$question['question'],'required'=>'required','label'=>false,'div'=>false));?>
																	</th>
																<?php endforeach;?>
															</tr>
														<?php endforeach;?>
													<?php endforeach;?>
													
												</table>
											</div>
											<div class="clearfix"></div>
										<?php elseif($question['type']=='Multiple Choice'):?>
											<?php echo $question['question'];?>
											<div class="col-lg-12">
												<table class="table table-bordered">
													<tr>
														<?php $perc = (100 / count($question['Option']['OptionItem'])).'%';?>
													<?php foreach ($question['Option']['OptionItem'] as $key => $option):?>
														<?php $option_id = $question['id'].'-'.$option['id'];?>
														<td style="width:<?php echo $perc;?>;text-align:center"><input id="<?php echo $option_id;?>" type="radio" name="answer[<?php echo $question['id'];?>]" value="<?php echo $option['value'];?>" /></td>
													<?php endforeach;?>
													</tr>
													<tr>
														<?php $perc = (100 / count($question['Option']['OptionItem'])).'%';?>
													<?php foreach ($question['Option']['OptionItem'] as $key => $option):?>
														<?php $option_id = $question['id'].'-'.$option['id'];?>
														<td style="width:<?php echo $perc;?>;text-align:center"><label for="<?php echo $option_id;?>"><?php echo $option['value'];?></label></td>
													<?php endforeach;?>
													</tr>
												</table>
											</div>
										<?php endif;?>
									</li>
								<?php endforeach;?>
							</ul>
						</li>
					<?php endforeach;?>
				</ol>
			<?php else:?>
				<ul style="list-style: none outside none;">
					<?php foreach ($form['Question'] as $num => $question):?>
						<li>
							<?php if($question['type']=='Short Answer'):?>
								<?php echo $this->Form->input('answer['.$question['id'].']',array('name'=>'answer['.$question['id'].']','label'=>$question['question'],'required'=>'required'));?>
								<div class="clearfix"></div>
							<?php elseif($question['type']=='Long Answer'):?>
								<?php echo $this->Form->input('answer['.$question['id'].']',array('name'=>'answer['.$question['id'].']','type'=>'textarea','label'=>$question['question'],'required'=>'required'));?>
								<div class="clearfix"></div>
							<?php elseif($question['type']=='Multiple Items'):?>
								<?php echo $question['question'];?>
								<div class="col-lg-offset-1 col-lg-10">
									<table class="table table-bordered">								
										<tr>
											<th>Items</th>
											<?php foreach ($question['Option']['OptionItem'] as $key => $item):?>
												<th><?php echo $item['value'];?></th>
											<?php endforeach;?>
										</tr>								
										<?php foreach ($question['QuestionItem'] as $key => $question_item):?>
											<tr>
												<th style="white-space:nowrap"><?php echo $question_item['title'];?></th>
												<?php foreach ($question['Option']['OptionItem'] as $key => $item):?>
													<?php if(count($question_item['ChildQuestionItem']) < 1):?>
													<th>
														<?php echo $this->Form->input('answer['.$question['id'].']['.$question_item['id'].']['.$item['id'].']',array('name'=>'answer['.$question['id'].']['.$question_item['id'].']['.$item['id'].']','label'=>$question['question'],'required'=>'required','label'=>false,'div'=>false));?>
													</th>
													<?php endif;?>
												<?php endforeach;?>
											</tr>
											<?php foreach ($question_item['ChildQuestionItem'] as $key => $question_subitem):?>
												<tr>
													<th style="padding-left:30px;white-space:nowrap"><?php echo $question_subitem['title'];?></th>
													<?php foreach ($question['Option']['OptionItem'] as $key => $item):?>
														<th>
															<?php echo $this->Form->input('answer['.$question['id'].']['.$question_subitem['id'].']['.$item['id'].']',array('name'=>'answer['.$question['id'].']['.$question_subitem['id'].']['.$item['id'].']','label'=>$question['question'],'required'=>'required','label'=>false,'div'=>false));?>
														</th>
													<?php endforeach;?>
												</tr>
											<?php endforeach;?>
										<?php endforeach;?>
										
									</table>
								</div>
							<?php elseif($question['type']=='Multiple Choice'):?>
								<?php echo $question['question'];?>
								<div class="col-lg-12">
									<table class="table table-bordered">
										<tr>
											<?php $perc = (100 / count($question['Option']['OptionItem'])).'%';?>
										<?php foreach ($question['Option']['OptionItem'] as $key => $option):?>
											<?php $option_id = $question['id'].'-'.$option['id'];?>
											<td style="width:<?php echo $perc;?>;text-align:center"><input id="<?php echo $option_id;?>" type="radio" name="answer[<?php echo $question['id'];?>]" value="<?php echo $option['value'];?>" /></td>
										<?php endforeach;?>
										</tr>
										<tr>
											<?php $perc = (100 / count($question['Option']['OptionItem'])).'%';?>
										<?php foreach ($question['Option']['OptionItem'] as $key => $option):?>
											<?php $option_id = $question['id'].'-'.$option['id'];?>
											<td style="width:<?php echo $perc;?>;text-align:center"><label for="<?php echo $option_id;?>"><?php echo $option['value'];?></label></td>
										<?php endforeach;?>
										</tr>
									</table>
								</div>
							<?php endif;?>
						</li>
					<?php endforeach;?>
				</ul> 
			<?php endif;?> 
			
		</li>
	
	<?php endforeach;?>  
	</ol>
	<div id="demoNavigation" class="pull-right"> 							
		<input class="navigation_button btn btn-default" id="back" value="Back" type="reset" />
		<input class="navigation_button btn btn-default" id="next" value="Next" type="submit" />
	</div>
	<div class="clearfix"></div>
</div>
<?php echo $this->Form->end(); ?>
<script type="text/javascript">
	$(function(){
		$("#SurveyForm").formwizard({ 
		 	formPluginEnabled: true,
		 	validationEnabled: true,
		 	focusFirstInput : true,
		 	formOptions :{
				success: function(data){
					alert('success');
					window.location.reload();
				},
				//beforeSubmit: function(data){$("#data").html("data sent to the server: " + $.param(data));},
				dataType: 'json',
				resetForm: true
		 	}	
		 }
		);
	});
</script>