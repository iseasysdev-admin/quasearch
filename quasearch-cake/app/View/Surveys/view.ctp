
<h1><?php echo $survey['Survey']['title'];?></h1>
<?php echo $this->Form->create('Survey', array('id' => 'SurveyForm','class'=>'form-horizontal')); ?> 
<?php echo $this->Form->hidden('survey_id',array('value'=>$survey['Survey']['id']));?>
<div id="fieldWrapper" class="col-lg-12">
	<ol>
	<?php foreach ($forms as $key => $form):?>
		<li class="step" id="<?php echo $form['SurveyForm']['id'];?>">
			<?php echo $form['SurveyForm']['title'];?>
			<?php if(!empty($form['forms'])):?>
			<ol >
				<?php foreach($form['forms'] as $form):?>
					<li type="square">
						<?php echo $form['SurveyForm']['title'];?>
						<ol style="list-style: none outside none;">				
							<?php foreach ($form['Question'] as $num => $question):?>
								<?php
									$form_id = $form['SurveyForm']['id'];
									$question_id = $question['id'];
									$statistic = $statistics['questionnaires'][$form_id][$question_id]
								?>
								<li>
									<?php echo $statistic['question'];?>

									<?php if($statistic['type']=='Multiple Choice'):?>
										<div class="col-lg-12">
											<table class="table table-bordered"> 
												<tr>
													<?php $perc = (100 / count($question['Option']['OptionItem'])).'%';?>
												<?php foreach ($question['Option']['OptionItem'] as $key => $option):?>
													<?php $option_id = $question['id'].'-'.$option['id'];?>
													<td style="width:<?php echo $perc;?>;text-align:center"><label for="<?php echo $option_id;?>"><?php echo $option['value'];?></label></td>
												<?php endforeach;?>
												</tr>
												<tr class="question">
													<?php $perc = (100 / count($question['Option']['OptionItem'])).'%';?>
												<?php foreach ($question['Option']['OptionItem'] as $key => $option):?>
													<?php $option_id = $question['id'].'-'.$option['id'];?>
													<td style="width:<?php echo $perc;?>;text-align:center" class="answer">
														<span class="badge"><?php echo $statistic['counts'][$option['id']];?></span>
													</td>
												<?php endforeach;?>
												</tr>
											</table>
										</div>
									<?php elseif($statistic['type']=='Multiple Items'):?>

										<div class="clearfix"></div><br>
										<div class="col-lg-offset-1 col-lg-10">
											<table class="table table-bordered">								
												<tr>
													<th>Items</th>
													<?php foreach ($question['Option']['OptionItem'] as $key => $item):?>
														<th><?php echo $item['value'];?></th>
													<?php endforeach;?>
												</tr>
												<?php foreach ($question['QuestionItem'] as $question_item_id => $question_item):?>
													
													<tr class="question">
														<th style=""><?php echo $question_item['title'];?></th>											
														<?php if(count($question_item['ChildQuestionItem']) < 1):?> 
															<?php 
																$counts = $statistic[$question_item['id']]['counts'];
															?>
															<?php foreach ($question['Option']['OptionItem'] as $key => $item):?>
															<th class="answer">
																<span class="badge"><?php echo $counts[$item['id']];?></span>
															</th>
															<?php endforeach;?>
															
														<?php endif;?>
														
													</tr>
													<?php foreach ($question_item['ChildQuestionItem'] as $key => $question_subitem):?>
														<tr class="question">
															<th style="padding-left:30px;white-space:nowrap"><?php echo $question_subitem['title'];?></th>
															<?php 
																$counts = $statistic[$question_item['id']][$question_subitem['id']]['counts'];
																
															?>
															<?php foreach ($question['Option']['OptionItem'] as $key => $item):?>
																<th class="answer">
																	<span class="badge"><?php echo $counts[$item['id']];?></span>
																</th>
															<?php endforeach;?>
														</tr>
													<?php endforeach;?>
												<?php endforeach;?>
												
											</table>
										</div>
									<?php else:?>
										<a href="<?php echo $this->Html->url(array('controller'=>'submissions','action'=>'byquestion',$question_id));?>">
											<span class="badge badge-info"><?php echo $statistic['count'];?></span>
										</a>
									<?php endif;?>
								</li>
							<?php endforeach;?>
						</ol>
						<div class="clearfix"></div><br>
					</li>
				<?php endforeach;?>
			</ol>
			<?php else:?>
				<ol style="list-style: none outside none;">				
					<?php foreach ($form['Question'] as $num => $question):?>
						<?php
							$form_id = $form['SurveyForm']['id'];
							$question_id = $question['id'];
							$statistic = $statistics['questionnaires'][$form_id][$question_id]
						?>
						<li>
							<?php echo $statistic['question'];?>

							<?php if($statistic['type']=='Multiple Choice'):?>
								<div class="col-lg-12">
									<table class="table table-bordered"> 
										<tr>
											<?php $perc = (100 / count($question['Option']['OptionItem'])).'%';?>
										<?php foreach ($question['Option']['OptionItem'] as $key => $option):?>
											<?php $option_id = $question['id'].'-'.$option['id'];?>
											<td style="width:<?php echo $perc;?>;text-align:center"><label for="<?php echo $option_id;?>"><?php echo $option['value'];?></label></td>
										<?php endforeach;?>
										</tr>
										<tr class="question">
											<?php $perc = (100 / count($question['Option']['OptionItem'])).'%';?>
										<?php foreach ($question['Option']['OptionItem'] as $key => $option):?>
											<?php $option_id = $question['id'].'-'.$option['id'];?>
											<td style="width:<?php echo $perc;?>;text-align:center" class="answer">
												<span class="badge"><?php echo $statistic['counts'][$option['id']];?></span>
											</td>
										<?php endforeach;?>
										</tr>
									</table>
								</div>
							<?php elseif($statistic['type']=='Multiple Items'):?>

								<div class="clearfix"></div><br>
								<div class="col-lg-offset-1 col-lg-10">
									<table class="table table-bordered">								
										<tr>
											<th>Items</th>
											<?php foreach ($question['Option']['OptionItem'] as $key => $item):?>
												<th><?php echo $item['value'];?></th>
											<?php endforeach;?>
										</tr>
										<?php foreach ($question['QuestionItem'] as $question_item_id => $question_item):?>
											
											<tr class="question">
												<th style="white-space:nowrap"><?php echo $question_item['title'];?></th>											
												<?php if(count($question_item['ChildQuestionItem']) < 1):?> 
													<?php 
														$counts = $statistic[$question_item['id']]['counts'];
													?>
													<?php foreach ($question['Option']['OptionItem'] as $key => $item):?>
													<th class="answer">
														<span class="badge"><?php echo $counts[$item['id']];?></span>
													</th>
													<?php endforeach;?>
													
												<?php endif;?>
												
											</tr>
											<?php foreach ($question_item['ChildQuestionItem'] as $key => $question_subitem):?>
												<tr class="question">
													<th style="padding-left:30px;white-space:nowrap"><?php echo $question_subitem['title'];?></th>
													<?php 
														$counts = $statistic[$question_item['id']][$question_subitem['id']]['counts'];
														
													?>
													<?php foreach ($question['Option']['OptionItem'] as $key => $item):?>
														<th class="answer">
															<span class="badge"><?php echo $counts[$item['id']];?></span>
														</th>
													<?php endforeach;?>
												</tr>
											<?php endforeach;?>
										<?php endforeach;?>
										
									</table>
								</div>
							<?php else:?>
								<a href="<?php echo $this->Html->url(array('controller'=>'submissions','action'=>'byquestion',$question_id));?>">
									<span class="badge badge-info"><?php echo $statistic['count'];?></span>
								</a>
							<?php endif;?>
						</li>
					<?php endforeach;?>
				</ol>
			<?php endif;?> 
			
		</li>
	
	<?php endforeach;?> 
</div>
<?php echo $this->Form->end(); ?> 
<script type="text/javascript">
	$(".question").each(function(){
	  var highest = -Infinity;
	  $(this).find('.answer').each(function(){
	    var val = $.trim($(this).text());
	    highest = Math.max(highest, val);
	  });
	  
	  $(this).find('.answer').each(function(){
	    var val = $.trim($(this).text());
	    if(val==highest){
	       $(this).find('.badge').addClass('label-success') 
	    }
	  });
	  //console.log(highest);
	});
</script>