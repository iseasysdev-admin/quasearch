<div class="submissions view">
<h2><?php echo __('Submission'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($submission['Submission']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Enumerator'); ?></dt>
		<dd>
			<?php echo h($submission['Submission']['enumerator']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Location'); ?></dt>
		<dd>
			<?php  
				$request = file_get_contents($submission['Submission']['map_api_url']);
				$json = json_decode($request, true);
				echo $json['results'][0]['formatted_address'];
			?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($submission['Submission']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($submission['Submission']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($submission['Submission']['status']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="related">
	<h3><?php echo __('Related Submission Details'); ?></h3> 
<h1><?php echo $survey['Survey']['title'];?></h1> 

<div id="fieldWrapper" class="col-lg-12">
	<ol>
	<?php foreach ($forms as $key => $form):?>
		<li class="step" id="<?php echo $form['SurveyForm']['id'];?>">			
			<h2><?php echo $form['SurveyForm']['title'];?></h2>
			<ol style="list-style:square" >
			<?php if(!empty($form['forms'])):?>
			
				<?php foreach($form['forms'] as $form):?>
					<li type="square">
						<?php echo $form['SurveyForm']['title'];?>
						<ol style="list-style: none outside none;">				
							<?php foreach ($form['Question'] as $num => $question):?>
							<?php 
								$answer = $responses[$question['id']]; 
							?>
								<?php if($question['type']=='Multiple Items'):?>
								<li>
									<?php echo $question['question'];?><br/>
									<div class="col-lg-offset-1 col-lg-10">
										<table class="table table-bordered">								
											<tr>
												<th>Items</th>
												<?php foreach ($question['Option']['OptionItem'] as $key => $item):?>
													<th style="text-align:center"><?php echo $item['value'];?></th>
												<?php endforeach;?>
											</tr>								
											<?php foreach ($question['QuestionItem'] as $key => $question_item):?>
												<tr>
													<td style="white-space:nowrap"><?php echo $question_item['title'];?></td>
													<?php if(count($question_item['ChildQuestionItem']) < 1):?>
														<?php

															$item_responses = $answer['answer'][$question_item['id']];
															
														?>
														<?php foreach ($item_responses as $key => $value):?>
															<td>														
																<span class="text text-info"><?php echo $value;?></span>
															</td>
														<?php endforeach;?>
													<?php endif;?>
												</tr>
												
												<?php foreach ($question_item['ChildQuestionItem'] as $key => $question_subitem):?>
													<?php
														$item_responses = $answer['answer'][$question_subitem['id']];
													?>
													<tr>
														<td style="padding-left:30px;white-space:nowrap"><?php echo $question_subitem['title'];?></td>
														<?php foreach ($item_responses as $key => $value):?>
															<td>														
																<span class="text text-info"><?php echo $value;?></span>
															</td>
														<?php endforeach;?>
													</tr>
												<?php endforeach;?>
											<?php endforeach;?>
											
										</table>
									</div>
									<div class="clearfix"></div>
								</li>
								<?php else:?>
								<li>
									<?php echo $answer['question'];?><br/>
									<span class="text text-info"><?php echo $answer['answer'];?></span>
									<div class="clearfix"></div>
									
									<?php if(!empty($answer['audio'])):?>
										<video controls>
										  <source src="/files/<?php echo $submission['Submission']['id'];?>/<?php echo $answer['audio'];?>" type="video/mp4">

										  Your browser does not support the <code>video</code> element.
										</video>
									<?php endif;?>
								</li>
								<?php endif;?>
							<?php endforeach;?>
						</ol>	
					</li>
				<?php endforeach;?>
			
			<?php else:?>
				<?php foreach ($form['Question'] as $num => $question):?>
				<?php 
					$answer = $responses[$question['id']]; 
				?>
				<li>
				<?php if($question['type']=='Multiple Items'):?>
					<?php echo $question['question'];?><br/>
					<div class="col-lg-offset-1 col-lg-10">
						<table class="table table-bordered">								
							<tr>
								<th>Items</th>
								<?php foreach ($question['Option']['OptionItem'] as $key => $item):?>
									<th style="text-align:center"><?php echo $item['value'];?></th>
								<?php endforeach;?>
							</tr>								
							<?php foreach ($question['QuestionItem'] as $key => $question_item):?>
								<tr>
									<td style="white-space:nowrap"><?php echo $question_item['title'];?></td>
									<?php if(count($question_item['ChildQuestionItem']) < 1):?>
										<?php

											$item_responses = $answer['answer'][$question_item['id']];
											
										?>
										<?php foreach ($item_responses as $key => $value):?>
											<td>														
												<span class="text text-info"><?php echo $value;?></span>
											</td>
										<?php endforeach;?>
									<?php endif;?>
								</tr>
								
								<?php foreach ($question_item['ChildQuestionItem'] as $key => $question_subitem):?>
									<?php
										$item_responses = $answer['answer'][$question_subitem['id']];
									?>
									<tr>
										<td style="padding-left:30px;white-space:nowrap"><?php echo $question_subitem['title'];?></td>
										<?php foreach ($item_responses as $key => $value):?>
											<td>														
												<span class="text text-info"><?php echo $value;?></span>
											</td>
										<?php endforeach;?>
									</tr>
								<?php endforeach;?>
							<?php endforeach;?>
							
						</table>
					</div>

				<?php else:?>
					<?php echo $answer['question'];?><br/>
					<span class="text text-info"><?php echo $answer['answer'];?></span>
					
					<?php if(!empty($answer['audio'])):?>
						<video controls>
						  <source src="/files/<?php echo $submission['Submission']['id'];?>/<?php echo $answer['audio'];?>" type="video/mp4">

						  Your browser does not support the <code>video</code> element.
						</video>
					<?php endif;?>
				<?php endif;?>
					<div class="clearfix"></div>
				</li>
				<?php endforeach;?>
			<?php endif;?>
			</ol>
		</li>	
	<?php endforeach;?> 
	</ol>
	<div class="clearfix"></div>
	<div class="hero-unit" style="text-align:center">
		<div class="thumbnail">			
			<img src="/files/<?php echo $submission['Submission']['id'];?>/<?php echo $submission['Submission']['image_encoding'];?>" />
		</div>
	</div>
</div>
<?php echo $this->Form->end(); ?>  
</div>