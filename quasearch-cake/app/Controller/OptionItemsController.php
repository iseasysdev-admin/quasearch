<?php
App::uses('AppController', 'Controller');
/**
 * OptionItems Controller
 *
 * @property OptionItem $OptionItem
 * @property PaginatorComponent $Paginator
 */
class OptionItemsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->OptionItem->recursive = 0;
		$this->set('optionItems', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->OptionItem->exists($id)) {
			throw new NotFoundException(__('Invalid option item'));
		}
		$options = array('conditions' => array('OptionItem.' . $this->OptionItem->primaryKey => $id));
		$this->set('optionItem', $this->OptionItem->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->OptionItem->create();
			if ($this->OptionItem->save($this->request->data)) {
				$this->Session->setFlash(__('The option item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The option item could not be saved. Please, try again.'));
			}
		}
		$options = $this->OptionItem->Option->find('list');
		$this->set(compact('options'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->OptionItem->exists($id)) {
			throw new NotFoundException(__('Invalid option item'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->OptionItem->save($this->request->data)) {
				$this->Session->setFlash(__('The option item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The option item could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('OptionItem.' . $this->OptionItem->primaryKey => $id));
			$this->request->data = $this->OptionItem->find('first', $options);
		}
		$options = $this->OptionItem->Option->find('list');
		$this->set(compact('options'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->OptionItem->id = $id;
		if (!$this->OptionItem->exists()) {
			throw new NotFoundException(__('Invalid option item'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->OptionItem->delete()) {
			$this->Session->setFlash(__('The option item has been deleted.'));
		} else {
			$this->Session->setFlash(__('The option item could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
