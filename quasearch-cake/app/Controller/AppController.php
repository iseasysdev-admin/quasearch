<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	var $helpers = array('Html','Js','Session','Form'=> array('className' => 'TwitterBootstrap.BootstrapForm'),'Text','Time');
    var $components = array(
    	'Paginator',
    	'Session',
    	'DebugKit.Toolbar',
    	'RequestHandler',
    	'Acl',
        'Auth' => array(
            'authorize' => array('Controller') // Added this line
        )
    );

    public $paginate = array(
        'limit' => 10
    );

	public function beforeFilter(){
		parent::beforeFilter();
		Configure::load('application');

		//Configure AuthComponent
        $this->Auth->loginAction = array(
          'controller' => 'users',
          'action' => 'login'
        );
        $this->Auth->logoutRedirect = array(
          'controller' => 'users',
          'action' => 'login'
        );

        //allow all
        $this->Auth->allow();
        $this->Auth->allow('logout');//'display',
        if( $this->Auth->user() 
          && !empty( $this->request->data ) 
          && empty( $this->request->data[$this->Auth->userModel]) 
          && ($this->request->params['controller'] != 'users' ) ) {                    
          $this->request->data['User'] = $this->currentUser();
        }
	}

  	public function beforeRender(){

      foreach ($this->uses as $key => $class) {
        if(strpos($class, '.')!=false) continue;
        $this->set($this->{$class}->enumValues());  
      }

	    if($this->Auth->user()){
	      $this->theme = $this->Session->read('Group.name');
	    }

      if($this->request->params['action']=='perform'){
        $this->theme = 'Mobile';
      }
	    
	  }

    protected function currentUser() {
      $user = $this->Auth->user();      
      return $user; # Return the complete user array
    }
 
	function isAuthorized($user) {
	// return false;
	return $this->Auth->loggedIn();
	}
}
