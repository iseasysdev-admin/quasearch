<?php
App::uses('AppController', 'Controller');
/**
 * QuestionItems Controller
 *
 * @property QuestionItem $QuestionItem
 * @property PaginatorComponent $Paginator
 */
class QuestionItemsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->QuestionItem->recursive = 0;
		$this->set('questionItems', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->QuestionItem->exists($id)) {
			throw new NotFoundException(__('Invalid question item'));
		}
		$options = array('conditions' => array('QuestionItem.' . $this->QuestionItem->primaryKey => $id));
		$this->set('questionItem', $this->QuestionItem->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->QuestionItem->create();
			if ($this->QuestionItem->save($this->request->data)) {
				$this->Session->setFlash(__('The question item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The question item could not be saved. Please, try again.'));
			}
		}
		$parentParams['conditions']['parent_id <'] =1;
		$parentQuestionItems = $this->QuestionItem->ParentQuestionItem->find('list',$parentParams);
		$parentQuestionItems = array_merge(array('0'=>''),$parentQuestionItems);
		$questionParams['conditions']['type'] = 'Multiple Items';
		$questionParams['fields'] = array('Question.question');
		$questions = $this->QuestionItem->Question->find('list',$questionParams);
		$this->set(compact('parentQuestionItems', 'questions'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->QuestionItem->exists($id)) {
			throw new NotFoundException(__('Invalid question item'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->QuestionItem->save($this->request->data)) {
				$this->Session->setFlash(__('The question item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The question item could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('QuestionItem.' . $this->QuestionItem->primaryKey => $id));
			$this->request->data = $this->QuestionItem->find('first', $options);
		}
		$parentQuestionItems = $this->QuestionItem->ParentQuestionItem->find('list');
		$questions = $this->QuestionItem->Question->find('list');
		$this->set(compact('parentQuestionItems', 'questions'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->QuestionItem->id = $id;
		if (!$this->QuestionItem->exists()) {
			throw new NotFoundException(__('Invalid question item'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->QuestionItem->delete()) {
			$this->Session->setFlash(__('The question item has been deleted.'));
		} else {
			$this->Session->setFlash(__('The question item could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
