<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	//public $components = array('Paginator');

	public function login() {
		$this->layout = 'login';
	    if ($this->request->is('post')) {
	    	
	    	//find user	    	
	    	$conditions['username'] = $this->request->data['User']['username'];
	    	$user = $this->User->find('first',array('conditions'=>$conditions));
	    	$loginLog['username'] = $this->request->data['User']['username'];
	    	$loginLog['ip_address'] = $this->request->clientIp();
	        if (!empty($user)) {	        	
	        	$loginLog['user_id'] = $user['User']['id'];

	        	if($user['User']['password']==$this->request->data['User']['password']){        			
        			$loginLog['result'] = 'success';
	        		$this->User->LoginLog->save($loginLog);
		        	$this->Auth->login($user['User']);	   
		        	$this->Session->setFlash("Login Successful");     			
		        	return $this->redirect('/');
		            
	        	}else if($user['User']['password']!=$this->request->data['User']['password']){
	        		$loginLog['result'] = 'Login Failed: Invalid Password';
	        		$error = __('Invalid username or password, try again');
	        	}
	        }else{
	        	$loginLog['result'] = 'Login Failed: Unknown Username';	
	        	$error = __('Invalid username or password, try again');
	        }
	        
	        $this->User->LoginLog->save($loginLog);
	        $this->Session->setFlash($error,'flash_error');
	    }	    
	    
	}

	public function logout() {
		$this->Session->setFlash("You have been logged out.");    
	    return $this->redirect($this->Auth->logout());
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {

		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}

		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));

		$this->render('form');
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if(empty($this->request->data['User']['password'])){
				unset($this->request->data['User']['password']);
				unset($this->request->data['User']['confirm_password']);
			}
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));

		$this->render('form');
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
