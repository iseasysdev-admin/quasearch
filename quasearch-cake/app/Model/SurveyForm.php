<?php
App::uses('AppModel', 'Model');
/**
 * SurveyForm Model
 *
 * @property Survey $Survey
 * @property User $User
 */
class SurveyForm extends AppModel {

	public function __construct($id = false, $table = null, $ds = null) {
	    parent::__construct($id, $table, $ds);
	    $this->order = sprintf('%s.parent_id', $this->alias).' ASC,'.sprintf('%s.sequence_no', $this->alias).' ASC';
	    //$this->order = sprintf('%s.sequence_no', $this->alias).' ASC,'.sprintf('%s.parent_id', $this->alias).' ASC';
	}

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'survey_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'title' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'description' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'status' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Survey' => array(
			'className' => 'Survey',
			'foreignKey' => 'survey_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ParentSurveyForm' => array(
			'className' => 'SurveyForm',
			'foreignKey' => 'parent_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $hasMany = array(
		'Question' => array(
			'className' => 'Question',
			'foreignKey' => 'survey_form_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterCache' => true
		),
		'ChildSurveyForm' => array(
			'className' => 'SurveyForm',
			'foreignKey' => 'parent_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


	public function listOptions($params = array()){
		$params['contain'] = array('ParentSurveyForm');		
		$categories = $this->find('all',$params);
		$options[0] = '';
		foreach ($categories as $key => $cat) {
			$option_title = $cat['SurveyForm']['title'];
			if($cat['SurveyForm']['parent_id'] > 0){
				$option_title = $cat['ParentSurveyForm']['title'].' -> '.$cat['SurveyForm']['title'];
			}
			$option_id = $cat['SurveyForm']['id'];

			$options[$option_id] = $option_title;
		} 

		return $options;

	}
}
