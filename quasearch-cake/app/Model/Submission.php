<?php
App::uses('AppModel', 'Model');
/**
 * Submission Model
 *
 * @property User $User
 * @property SubmissionDetail $SubmissionDetail
 */
class Submission extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'raw_data' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'status' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'SubmissionDetail' => array(
			'className' => 'SubmissionDetail',
			'foreignKey' => 'submission_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'QuestionAnswer' => array(
			'className' => 'QuestionAnswer',
			'foreignKey' => 'submission_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'QuestionItemAnswer' => array(
			'className' => 'QuestionItemAnswer',
			'foreignKey' => 'submission_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public function json_decode_array($input) {
	  	$from_json =  json_decode($input, true);  
	  	return $from_json ? $from_json : $input; 
	}

	public function afterSave($created, $options = array()) {
		if ($created) {
			
			$QuestionAnswer = $this->QuestionAnswer;			
			$QuestionItemAnswer = $this->QuestionItemAnswer;
			//cleanup answers
			$QuestionAnswer->deleteAll(array('submission_id'=>$this->id));

			
			$submission = $this->json_decode_array($this->data['Submission']['raw_data']);
			$answers = $submission['answer'];		
	
			$question_answers = array();
			foreach ($answers as $question_id => $answer) {
				$question_answer['submission_id'] = $this->id;

				if(! is_array($answer)){
					$question_answer['question_id'] = $question_id;
					$question_answer['value'] = $answer;
					$QuestionAnswer->create();
					$QuestionAnswer->save($question_answer,false);
				}else{
					//Multiple Items - Question
					//$question_item_answer['question_id'] = $question_id;
					$question_item_answer['submission_id'] = $this->id;

					
					$question_item_answer['submission_id'] = $this->id;

					foreach ($answer as $question_item_id => $question_item_data) {
						$question_item_answer['question_item_id'] = $question_item_id;						
						//cleanup item answers
						$QuestionItemAnswer->deleteAll(array('submission_id'=>$this->id,'question_item_id'=>$question_item_id));

						foreach ($question_item_data as $option_id => $answer) {							
							if($answer=='Yes'){
								$option_item = ClassRegistry::init('OptionItem')->read(null,$option_id);
								$question_item_answer['option_id'] = $option_id;
								$question_item_answer['value'] = $option_item['OptionItem']['value'];
								$QuestionItemAnswer->create();
								$QuestionItemAnswer->save($question_item_answer,false);
							}
						}
					}
				}
			}	
		}
	}

	public function getResponses($id){
		$submission = $this->read(null,$id);		
		$data = $this->json_decode_array(stripslashes($this->data['Submission']['raw_data'])); 
		$responses = array();
		App::uses('Question','Model');
		$Question = new Question();
		foreach ($data['answer'] as $question_id => $answer) {
			$question = $Question->read(null,$question_id);
			
			if(! is_object($answer)){
				$response['question'] = $question['Question']['question'];				
				$qaparams['conditions']['submission_id'] = $id;
				$qaparams['conditions']['question_id'] = $question_id;
				$qa = $this->QuestionAnswer->find('first',$qaparams);
				if(!empty($qa)){
					$response['audio'] = $qa['QuestionAnswer']['audio'];
					$response['answer'] = $qa['QuestionAnswer']['value'];
				}else{
					$response['answer'] = $answer;	
				}
				
				$responses[$question_id] = $response;

			}else{
				
				App::uses('QuestionItem','Model');
				$QuestionItem = new QuestionItem();
				//Multiple Items - Question								
				$question_item_responses = array();
				foreach ($answer as $question_item_id => $question_item_data) {
					
					$question_item_answer['question_item_id'] = $question_item_id;
					$question_item_responses[$question_item_id] = (array) $question_item_data;
					
					/*
					foreach ($question_item_data as $option_id => $answer) {						
						$question_item_response['option_id'] = $option_id;
						$question_item_response['value'] = $answer;

						$question_item_responses[]
					}*/
				}
				$response['question'] = $question['Question']['question'];
				$response['answer'] = $question_item_responses;
				$responses[$question_id] = $response;
			}
			
		}

		return $responses;

	}

}
