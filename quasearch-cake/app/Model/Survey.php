<?php
App::uses('AppModel', 'Model');
/**
 * Survey Model
 *
 * @property Form $Form
 * @property Question $Question
 */
class Survey extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'SurveyForm' => array(
			'className' => 'SurveyForm',
			'foreignKey' => 'survey_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public function getForm($form_id){
		$this->SurveyForm->contain(array('Question'=>array(
				'Option'=>'OptionItem',
				'QuestionItem'=>array('conditions'=>array('QuestionItem.parent_id'=>0),'ChildQuestionItem')
			)
		));
		$SurveyForm = $this->SurveyForm->read(null,$form_id);
		return $SurveyForm;
	}

	public function getStatistics($survey_id){
		$statistics = array();
		$this->contain(array('SurveyForm'=>array('order'=>array('SurveyForm.sequence_no'=>'ASC'))));
		$survey = $this->read(null,$survey_id);		
		$this->set(compact('survey'));
		$steps = array();
		foreach ($survey['SurveyForm'] as $key => $form) {			
			$form_id = $form['id'];
			$statistics['forms'][] = $form;
			$this->SurveyForm->Question->contain(array('Option'=>array('OptionItem')));
			$questionnaires = $this->SurveyForm->Question->find('all',array('conditions'=>array('survey_form_id'=>$form['id'])));
			
			foreach($questionnaires  as $question){
				
				$question_id = $question['Question']['id'];
				
				
				if($question['Question']['type']=='Multiple Items'){
					$statistic = array();
					$statistic['question'] = $question['Question']['question'];
					$statistic['type'] = $question['Question']['type'];
					$statistics['questionnaires'][$form_id][$question_id] = $statistic;

					$this->SurveyForm->Question->QuestionItem->contain(array('ChildQuestionItem'));
					$question_items = $this->SurveyForm->Question->QuestionItem->find('all',array('conditions'=>array('question_id'=>$question['Question']['id'])));
					foreach ($question_items as $key => $question_item) {
						$question_item_id = $question_item['QuestionItem']['id']; 
						if(count($question_item['ChildQuestionItem']) > 0){
							foreach ($question_item['ChildQuestionItem'] as $key => $question_sub_item) {
								
								$question_sub_item_id = $question_sub_item['id'];
								$statistic = array();
								$statistic['question'] = $question_sub_item['title'];

								foreach ($question['Option']['OptionItem'] as $key => $option) {									

									$params['conditions']['question_item_id'] = $question_sub_item['id'];
									$params['conditions']['option_id'] = $option['id'];									
									$params['conditions']['value'] = $option['value'];
									$statistic['counts'][$option['id']] = $this->SurveyForm->Question->QuestionItem->QuestionItemAnswer->find('count',$params);									
								}

								$statistics['questionnaires'][$form_id][$question_id][$question_item_id][$question_sub_item_id] = $statistic;
							}
						}else{
							$statistic = array();
							$statistic['question'] = $question_item['QuestionItem']['title'];

							foreach ($question['Option']['OptionItem'] as $key => $option) {									
								$params = array();
								$params['conditions']['question_item_id'] = $question_item['QuestionItem']['id'];
								$params['conditions']['option_id'] = $option['id'];
								$statistic['counts'][$option['id']] = $this->SurveyForm->Question->QuestionItem->QuestionItemAnswer->find('count',$params);																	
							} 
							$statistics['questionnaires'][$form_id][$question_id][$question_item_id] = $statistic;
						}

					}

				}else{
					$statistic = array();
					$statistic['question'] = $question['Question']['question'];
					$statistic['type'] = $question['Question']['type']; 
					if(empty($question['Option']['id'])){
						$statistic['count'] = $this->SurveyForm->Question->QuestionAnswer->find('count',array('conditions'=>array('question_id'=>$question['Question']['id'],'QuestionAnswer.value !='=>'')));
					}else{

						foreach ($question['Option']['OptionItem'] as $key => $option) {									
							$params = array();
							$params['conditions']['question_id'] = $question['Question']['id'];
							$params['conditions']['value'] = $option['value'];

							$statistic['counts'][$option['id']] = $this->SurveyForm->Question->QuestionAnswer->find('count',$params);							
							
						}
						
						$statistics['questionnaires'][$form_id][$question_id][$question_item_id] = $statistic;

					}
					
					$statistics['questionnaires'][$form_id][$question_id] = $statistic;
				}
			}
		}
		//pr($statistics);
		$this->set(compact('forms'));
		return $statistics;

	}

}
