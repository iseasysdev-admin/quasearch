<?php
App::uses('ApiAppModel', 'Api.Model');
/**
 * Recipe Model
 *
 */
class Recipe extends ApiAppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
