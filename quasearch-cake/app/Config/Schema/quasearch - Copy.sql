-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 08, 2014 at 04:45 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quasearch`
--

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

DROP TABLE IF EXISTS `acos`;
CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=316 ;

--
-- Dumping data for table `acos`
--

INSERT INTO `acos` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(302, NULL, NULL, NULL, '', 615, 630),
(303, 302, NULL, NULL, 'Teams', 616, 629),
(304, 303, NULL, NULL, 'index', 617, 618),
(305, 303, NULL, NULL, 'view', 619, 620),
(306, 303, NULL, NULL, 'add', 621, 622),
(307, 303, NULL, NULL, 'edit', 623, 624),
(308, 303, NULL, NULL, 'delete', 625, 626),
(309, 303, NULL, NULL, 'currentUser', 627, 628),
(310, 226, NULL, NULL, 'login', 469, 470),
(311, 226, NULL, NULL, 'logout', 471, 472),
(312, 226, NULL, NULL, 'index', 473, 474),
(313, 226, NULL, NULL, 'view', 475, 476),
(314, 226, NULL, NULL, 'add', 477, 478),
(315, 226, NULL, NULL, 'edit', 479, 480);

-- --------------------------------------------------------

--
-- Table structure for table `aros`
--

DROP TABLE IF EXISTS `aros`;
CREATE TABLE IF NOT EXISTS `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, 'Group', 1, NULL, 1, 6),
(2, NULL, 'Group', 2, NULL, 7, 10),
(3, NULL, 'Group', 3, NULL, 11, 16),
(4, 2, 'User', 2, NULL, 8, 9),
(5, 3, 'User', 3, NULL, 12, 13),
(6, NULL, 'Group', 4, NULL, 17, 20),
(7, NULL, 'Group', 5, NULL, 21, 24),
(8, NULL, 'Group', 6, NULL, 25, 28),
(9, 1, 'User', 1, NULL, 2, 3),
(10, 3, 'User', 4, NULL, 14, 15),
(11, 6, 'User', 5, NULL, 18, 19),
(12, 7, 'User', 6, NULL, 22, 23),
(13, 8, 'User', 7, NULL, 26, 27),
(14, 1, 'Member', 1, NULL, 4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `aros_acos`
--

DROP TABLE IF EXISTS `aros_acos`;
CREATE TABLE IF NOT EXISTS `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `audits`
--

DROP TABLE IF EXISTS `audits`;
CREATE TABLE IF NOT EXISTS `audits` (
  `id` varchar(36) NOT NULL,
  `event` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `entity_id` varchar(36) NOT NULL,
  `json_object` text NOT NULL,
  `description` text,
  `source_id` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `audits`
--

INSERT INTO `audits` (`id`, `event`, `model`, `entity_id`, `json_object`, `description`, `source_id`, `created`) VALUES
('538c555c-57a4-47dc-bae7-14f86ff0fe6a', 'CREATE', 'Payment', '5', '{"Payment":{"id":"5","user_id":"1","invoice_id":"5","billing_id":"0","type":"Cash","payment_date":"2014-06-02","amount":"150.00","reference":"1235","check_number":null,"bank_name":null,"bank_branch":null,"notes":"","created":"2014-06-02 12:43:40","modified":"2014-06-02 12:43:40"}}', 'admin', '1', '2014-06-02 12:43:40'),
('538c555d-f4c4-4f25-b4de-14f86ff0fe6a', 'CREATE', 'Collection', '3', '{"Collection":{"id":"3","user_id":"0","category_id":"4","invoice_id":"5","homeowner_id":"1","collection_date":"2014-06-02","particular":"Vehicle Sticker ","amount":"150.00","time_from":"0000-00-00 00:00:00","time_to":"0000-00-00 00:00:00","created":"2014-06-02 12:43:40","modified":"2014-06-02 12:43:40","status":""}}', 'admin', '1', '2014-06-02 12:43:41'),
('538c55aa-7b44-4e8a-afe2-14f86ff0fe6a', 'CREATE', 'Payment', '6', '{"Payment":{"id":"6","user_id":"1","invoice_id":"0","billing_id":"0","type":"Cash","payment_date":"2014-06-02","amount":"3000.00","reference":"1236","check_number":null,"bank_name":null,"bank_branch":null,"notes":"","created":"2014-06-02 12:44:58","modified":"2014-06-02 12:44:58"}}', 'admin', '1', '2014-06-02 12:44:58'),
('53955a9d-a94c-4e8c-ae70-12506ff0fe6a', 'CREATE', 'Payment', '7', '{"Payment":{"id":"7","user_id":"1","invoice_id":"0","billing_id":"0","type":"Cash","payment_date":"2014-06-09","amount":"1334.04","reference":"1237","check_number":null,"bank_name":null,"bank_branch":null,"notes":"","created":"2014-06-09 08:56:29","modified":"2014-06-09 08:56:29"}}', 'admin', '1', '2014-06-09 08:56:29'),
('539563b2-4a3c-44b6-a199-12506ff0fe6a', 'CREATE', 'Discount', '1', '{"Discount":{"id":"1","homeowner_id":"1","user_id":"0","discount_date":"0000-00-00","particular":"Early Payment Discount","amount":"228.39","created":"2014-06-09 09:35:14"}}', 'admin', '1', '2014-06-09 09:35:14'),
('539563b2-d59c-462e-a290-12506ff0fe6a', 'CREATE', 'Payment', '8', '{"Payment":{"id":"8","user_id":"1","invoice_id":"0","billing_id":"10","type":"Cash","payment_date":"2014-06-09","amount":"3188.37","reference":"1238","check_number":null,"bank_name":null,"bank_branch":null,"notes":"","created":"2014-06-09 09:35:14","modified":"2014-06-09 09:35:14"}}', 'admin', '1', '2014-06-09 09:35:14'),
('539563c9-f19c-433c-adda-12506ff0fe6a', 'CREATE', 'Discount', '2', '{"Discount":{"id":"2","homeowner_id":"1","user_id":"0","discount_date":"0000-00-00","particular":"Early Payment Discount","amount":"228.39","created":"2014-06-09 09:35:37"}}', 'admin', '1', '2014-06-09 09:35:37'),
('5395649b-0dbc-4ec9-8d2b-12506ff0fe6a', 'CREATE', 'Discount', '3', '{"Discount":{"id":"3","homeowner_id":"1","user_id":"1","discount_date":"0000-00-00","particular":"Early Payment Discount","amount":"228.39","created":"2014-06-09 09:39:07"}}', 'admin', '1', '2014-06-09 09:39:07'),
('539564db-dd1c-481b-8744-12506ff0fe6a', 'CREATE', 'Discount', '4', '{"Discount":{"id":"4","homeowner_id":"1","user_id":"1","discount_date":"0000-00-00","particular":"Early Payment Discount","amount":"228.39","created":"2014-06-09 09:40:11"}}', 'admin', '1', '2014-06-09 09:40:11'),
('539564fd-801c-4a4c-95bc-12506ff0fe6a', 'CREATE', 'Discount', '5', '{"Discount":{"id":"5","homeowner_id":"1","user_id":"1","discount_date":"0000-00-00","particular":"Early Payment Discount","amount":"228.39","created":"2014-06-09 09:40:45"}}', 'admin', '1', '2014-06-09 09:40:45'),
('53956513-507c-4784-9c0b-12506ff0fe6a', 'CREATE', 'Discount', '6', '{"Discount":{"id":"6","homeowner_id":"1","user_id":"1","discount_date":"0000-00-00","particular":"Early Payment Discount","amount":"228.39","created":"2014-06-09 09:41:07"}}', 'admin', '1', '2014-06-09 09:41:07'),
('539565a3-337c-4a96-b6b3-12506ff0fe6a', 'CREATE', 'Discount', '7', '{"Discount":{"id":"7","homeowner_id":"1","user_id":"1","discount_date":"0000-00-00","particular":"Early Payment Discount","amount":"228.39","created":"2014-06-09 09:43:31"}}', 'admin', '1', '2014-06-09 09:43:31'),
('539565a3-cc9c-4908-b75c-12506ff0fe6a', 'CREATE', 'Payment', '9', '{"Payment":{"id":"9","user_id":"1","invoice_id":"0","billing_id":"11","type":"Cash","payment_date":"2014-06-09","amount":"3188.37","reference":"1239","check_number":null,"bank_name":null,"bank_branch":null,"notes":"","created":"2014-06-09 09:43:31","modified":"2014-06-09 09:43:31"}}', 'admin', '1', '2014-06-09 09:43:31'),
('53956694-5edc-415a-bc43-12506ff0fe6a', 'CREATE', 'Setting', '5', '{"Setting":{"id":"5","name":"Minimum Surchargeable Balance","description":"Minimum Surchargeable Balance","key":"surchargeable","value":"5000","created":"2014-06-09 09:47:32","modified":"2014-06-09 09:47:32"}}', 'admin', '1', '2014-06-09 09:47:32'),
('53956717-3f7c-4866-94c9-12506ff0fe6a', 'EDIT', 'Setting', '5', '{"Setting":{"id":"5","name":"Minimum Surchargeable Balance","description":"Minimum Surchargeable Balance","key":"surchargeable","value":"500","created":"2014-06-09 09:47:32","modified":"2014-06-09 09:49:43"}}', 'admin', '1', '2014-06-09 09:49:43'),
('5395675e-295c-4db2-b3a0-12506ff0fe6a', 'CREATE', 'Payment', '10', '{"Payment":{"id":"10","user_id":"1","invoice_id":"0","billing_id":"13","type":"Cash","payment_date":"2014-06-09","amount":"5000.00","reference":"1240","check_number":null,"bank_name":null,"bank_branch":null,"notes":"","created":"2014-06-09 09:50:54","modified":"2014-06-09 09:50:54"}}', 'admin', '1', '2014-06-09 09:50:54'),
('5395675e-479c-4557-aa55-12506ff0fe6a', 'CREATE', 'Discount', '8', '{"Discount":{"id":"8","homeowner_id":"1","user_id":"1","discount_date":"0000-00-00","particular":"Early Payment Discount","amount":"228.39","created":"2014-06-09 09:50:54"}}', 'admin', '1', '2014-06-09 09:50:54'),
('53956a5e-3cdc-4f95-94f9-12506ff0fe6a', 'CREATE', 'Payment', '11', '{"Payment":{"id":"11","user_id":"1","invoice_id":"0","billing_id":"15","type":"Cash","payment_date":"2014-06-09","amount":"423.28","reference":"1241","check_number":null,"bank_name":null,"bank_branch":null,"notes":"","created":"2014-06-09 10:03:42","modified":"2014-06-09 10:03:42"}}', 'admin', '1', '2014-06-09 10:03:42'),
('53956a5e-54bc-412a-b5da-12506ff0fe6a', 'CREATE', 'Discount', '9', '{"Discount":{"id":"9","homeowner_id":"1","user_id":"1","discount_date":"0000-00-00","particular":"Early Payment Discount","amount":"47.03","created":"2014-06-09 10:03:42"}}', 'admin', '1', '2014-06-09 10:03:42');

-- --------------------------------------------------------

--
-- Table structure for table `audit_deltas`
--

DROP TABLE IF EXISTS `audit_deltas`;
CREATE TABLE IF NOT EXISTS `audit_deltas` (
  `id` varchar(36) NOT NULL,
  `audit_id` varchar(36) NOT NULL,
  `property_name` varchar(255) NOT NULL,
  `old_value` text,
  `new_value` text,
  PRIMARY KEY (`id`),
  KEY `audit_id` (`audit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `audit_deltas`
--

INSERT INTO `audit_deltas` (`id`, `audit_id`, `property_name`, `old_value`, `new_value`) VALUES
('53956717-f8fc-499d-a978-12506ff0fe6a', '53956717-3f7c-4866-94c9-12506ff0fe6a', 'value', '5000', '500');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `created`, `status`) VALUES
(1, 'SuperAdministrator', '2014-05-06 20:41:24', 'active'),
(2, 'Administrator', '2014-05-06 20:41:47', 'active'),
(3, 'Staff', '2014-05-06 20:42:02', 'active'),
(4, 'SalesManager', '2014-07-01 12:19:42', 'active'),
(5, 'Broker', '2014-07-01 12:19:55', 'active'),
(6, 'Agent', '2014-07-01 12:20:29', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `login_logs`
--

DROP TABLE IF EXISTS `login_logs`;
CREATE TABLE IF NOT EXISTS `login_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `ip_address` varchar(40) NOT NULL,
  `result` text NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `login_logs`
--

INSERT INTO `login_logs` (`id`, `user_id`, `username`, `ip_address`, `result`, `created`) VALUES
(1, 0, 'kbitoon', '127.0.0.1', 'Login Failed: Unknown Username', '2014-06-23 04:55:39'),
(2, 0, 'kbitoon', '127.0.0.1', 'Login Failed: Unknown Username', '2014-06-23 04:56:51'),
(3, 1, 'admin', '127.0.0.1', 'success', '2014-06-23 04:57:22'),
(4, 1, 'admin', '::1', 'success', '2014-06-28 15:29:13'),
(5, 1, 'admin', '127.0.0.1', 'Login Failed: Invalid Password', '2014-07-01 11:30:47'),
(6, 1, 'admin', '127.0.0.1', 'Login Failed: Invalid Password', '2014-07-01 11:35:31'),
(7, 1, 'admin', '127.0.0.1', 'Login Failed: Invalid Password', '2014-07-01 11:35:40'),
(8, 1, 'admin', '127.0.0.1', 'success', '2014-07-01 11:35:54'),
(9, 1, 'admin', '127.0.0.1', 'success', '2014-07-02 13:03:52'),
(10, 1, 'admin', '127.0.0.1', 'success', '2014-07-04 07:00:24'),
(11, 1, 'admin', '127.0.0.1', 'success', '2014-07-05 07:03:20'),
(12, 1, 'admin', '::1', 'success', '2014-07-06 02:35:59'),
(13, 1, 'admin', '::1', 'success', '2014-07-06 06:36:33'),
(14, 1, 'admin', '::1', 'success', '2014-07-06 11:51:59'),
(15, 1, 'admin', '::1', 'success', '2014-07-06 15:53:07'),
(16, 1, 'admin', '::1', 'success', '2014-07-08 15:16:03');

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

DROP TABLE IF EXISTS `options`;
CREATE TABLE IF NOT EXISTS `options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `title`, `created`, `modified`, `status`) VALUES
(1, 'Quality Ratings', '2014-07-05 07:17:24', '2014-07-06 02:39:07', 'active'),
(2, 'Performance Ratings', '2014-07-06 02:40:49', '2014-07-06 02:40:49', 'active'),
(3, 'Occurence Ratings', '2014-07-06 02:43:34', '2014-07-06 02:43:34', 'active'),
(4, 'Satisfaction Ratings', '2014-07-06 02:45:15', '2014-07-06 02:45:15', 'active'),
(5, 'Agreement Options', '2014-07-06 03:22:47', '2014-07-06 03:23:03', 'active'),
(6, 'Training Attended fields', '2014-07-06 06:53:12', '2014-07-06 06:53:12', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `option_items`
--

DROP TABLE IF EXISTS `option_items`;
CREATE TABLE IF NOT EXISTS `option_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `option_items`
--

INSERT INTO `option_items` (`id`, `option_id`, `value`, `created`, `modified`, `status`) VALUES
(1, 1, 'Very High', '2014-07-05 07:24:11', '2014-07-05 07:24:11', 'active'),
(2, 1, 'A little above average', '2014-07-05 07:24:30', '2014-07-05 07:24:30', 'active'),
(3, 1, 'Average', '2014-07-05 07:24:39', '2014-07-05 07:24:39', 'active'),
(4, 1, 'A little below average', '2014-07-05 07:24:54', '2014-07-05 07:24:54', 'active'),
(5, 1, 'Very low', '2014-07-05 07:25:06', '2014-07-05 07:25:06', 'active'),
(6, 2, 'Much better than expected', '2014-07-06 02:41:47', '2014-07-06 02:41:47', 'active'),
(7, 2, 'Somewhat better than expected', '2014-07-06 02:42:00', '2014-07-06 02:42:00', 'active'),
(8, 2, 'Same as expected', '2014-07-06 02:42:12', '2014-07-06 02:42:12', 'active'),
(9, 2, 'Somewhat worse than expected', '2014-07-06 02:42:35', '2014-07-06 02:42:35', 'active'),
(10, 2, 'Much worse than expected', '2014-07-06 02:42:51', '2014-07-06 02:42:51', 'active'),
(11, 3, 'Always', '2014-07-06 02:44:01', '2014-07-06 02:44:01', 'active'),
(12, 3, 'Very Often', '2014-07-06 02:44:11', '2014-07-06 02:44:11', 'active'),
(13, 3, 'Sometimes', '2014-07-06 02:44:18', '2014-07-06 02:44:18', 'active'),
(14, 3, 'Rarely', '2014-07-06 02:44:26', '2014-07-06 02:44:26', 'active'),
(15, 3, 'Never', '2014-07-06 02:44:43', '2014-07-06 02:44:43', 'active'),
(16, 4, 'Very satisfied', '2014-07-06 02:45:40', '2014-07-06 02:45:40', 'active'),
(17, 4, 'Somewhat satisfied', '2014-07-06 02:45:51', '2014-07-06 02:45:51', 'active'),
(18, 4, 'Undecided', '2014-07-06 02:45:59', '2014-07-06 02:45:59', 'active'),
(19, 4, 'Somewhat dissatisfied', '2014-07-06 02:46:12', '2014-07-06 02:46:12', 'active'),
(20, 4, 'Very dissatisfied', '2014-07-06 02:46:24', '2014-07-06 02:46:24', 'active'),
(21, 5, 'Strongly Agree', '2014-07-06 03:26:31', '2014-07-06 03:26:31', 'active'),
(22, 5, 'Agree', '2014-07-06 03:26:45', '2014-07-06 03:26:45', 'active'),
(23, 5, 'Neutral', '2014-07-06 03:26:55', '2014-07-06 03:26:55', 'active'),
(24, 5, 'Disagree', '2014-07-06 03:27:06', '2014-07-06 03:27:06', 'active'),
(25, 5, 'Strongly Disagree', '2014-07-06 03:27:17', '2014-07-06 03:27:17', 'active'),
(26, 6, 'Relevant VisayasHealth products/services you are AWARE of', '2014-07-06 06:53:52', '2014-07-06 06:53:52', 'active'),
(27, 6, 'VisayasHealth products and services USED in the previous year', '2014-07-06 06:54:06', '2014-07-06 06:54:06', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `survey_form_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_sequence` int(11) DEFAULT NULL,
  `weight` int(10) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `survey_form_id`, `option_id`, `type`, `question`, `total_sequence`, `weight`, `created`, `modified`, `status`) VALUES
(1, 1, 0, 'Short Answer', 'Name of Respondent (optional)', NULL, 0, '2014-07-08 16:30:19', '2014-07-08 16:30:19', 'active'),
(2, 1, 0, 'Short Answer', 'Name of Organization', NULL, 0, '2014-07-08 16:30:33', '2014-07-08 16:30:33', 'active'),
(3, 1, 0, 'Long Answer', 'Address of Respondent/Organization', NULL, 0, '2014-07-08 16:30:48', '2014-07-08 16:30:48', 'active'),
(4, 1, 0, 'Long Answer', 'Position of Respondent in Organization', NULL, 0, '2014-07-08 16:31:02', '2014-07-08 16:31:02', 'active'),
(5, 3, 6, 'Multiple Items', 'What VisayasHealth Technical Assistance Packages are you aware of and have availed during the previous year (please check appropriate columns)', NULL, 0, '2014-07-08 16:32:34', '2014-07-08 16:32:34', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `question_answers`
--

DROP TABLE IF EXISTS `question_answers`;
CREATE TABLE IF NOT EXISTS `question_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `created` int(11) NOT NULL,
  `modified` int(11) NOT NULL,
  `stat` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `question_items`
--

DROP TABLE IF EXISTS `question_items`;
CREATE TABLE IF NOT EXISTS `question_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `question_items`
--

INSERT INTO `question_items` (`id`, `parent_id`, `question_id`, `title`, `created`, `modified`) VALUES
(1, 0, 5, 'Training of Providers', '2014-07-08 16:41:13', '2014-07-08 16:41:13'),
(2, 1, 5, 'Post-Partum IUD insertion', '2014-07-08 16:41:42', '2014-07-08 16:41:42');

-- --------------------------------------------------------

--
-- Table structure for table `question_item_answers`
--

DROP TABLE IF EXISTS `question_item_answers`;
CREATE TABLE IF NOT EXISTS `question_item_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) NOT NULL,
  `question_item_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `created` int(11) NOT NULL,
  `modified` int(11) NOT NULL,
  `stat` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `question_options`
--

DROP TABLE IF EXISTS `question_options`;
CREATE TABLE IF NOT EXISTS `question_options` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `question_id` int(10) NOT NULL,
  `sequence_id` int(10) DEFAULT NULL,
  `options` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` int(10) DEFAULT NULL,
  `point` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `submissions`
--

DROP TABLE IF EXISTS `submissions`;
CREATE TABLE IF NOT EXISTS `submissions` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) NOT NULL,
  `user_id` int(10) NOT NULL,
  `raw_data` text COLLATE utf8_unicode_ci NOT NULL,
  `point` int(10) DEFAULT NULL,
  `valid` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `submission_details`
--

DROP TABLE IF EXISTS `submission_details`;
CREATE TABLE IF NOT EXISTS `submission_details` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `question_id` int(10) NOT NULL,
  `submission_id` int(10) NOT NULL,
  `sequence_id` int(10) NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `point` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `surveys`
--

DROP TABLE IF EXISTS `surveys`;
CREATE TABLE IF NOT EXISTS `surveys` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `surveys`
--

INSERT INTO `surveys` (`id`, `title`, `created`, `modified`) VALUES
(1, 'CUSTOMER SERVICE ASSESSMENT - PARTNER LEVEL (DOH-RO: RD, ARD, MNCHN COORDINATOR)', '2014-07-08 15:24:10', '2014-07-08 15:24:10');

-- --------------------------------------------------------

--
-- Table structure for table `survey_forms`
--

DROP TABLE IF EXISTS `survey_forms`;
CREATE TABLE IF NOT EXISTS `survey_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0',
  `survey_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_profile` tinyint(1) NOT NULL DEFAULT '0',
  `sequence_no` int(11) NOT NULL DEFAULT '0',
  `question_count` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `survey_forms`
--

INSERT INTO `survey_forms` (`id`, `parent_id`, `survey_id`, `user_id`, `title`, `description`, `is_profile`, `sequence_no`, `question_count`, `created`, `modified`, `status`) VALUES
(1, 0, 1, 0, 'Organization Profile', 'Organization Profile', 0, 1, 4, '2014-07-08 15:25:01', '2014-07-08 15:37:30', 'active'),
(2, 0, 1, 0, 'Service Dimensions', 'Service Dimensions', 0, 2, 0, '2014-07-08 15:25:17', '2014-07-08 15:25:17', 'active'),
(3, 2, 1, 0, 'Utilization of Products and Services', 'Utilization of Products and Services', 0, 1, 1, '2014-07-08 15:31:18', '2014-07-08 15:31:18', 'active'),
(4, 2, 1, 0, 'Technical assistance', 'Technical assistance', 0, 2, 0, '2014-07-08 15:34:56', '2014-07-08 15:44:26', 'active'),
(5, 2, 1, 0, 'Service Orientation', 'Service Orientation', 0, 3, 0, '2014-07-08 15:38:48', '2014-07-08 15:44:46', 'active'),
(6, 2, 1, 0, 'Program relevance', 'Program relevance', 0, 4, 0, '2014-07-08 15:39:03', '2014-07-08 15:45:03', 'active'),
(7, 2, 1, 0, 'Project Administration', 'Project Administration', 0, 5, 0, '2014-07-08 15:39:23', '2014-07-08 15:45:19', 'active'),
(8, 0, 1, 0, 'Overall Customer Service Rating', 'Overall Customer Service Rating', 0, 3, 0, '2014-07-08 15:39:44', '2014-07-08 15:45:33', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(80) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `group_id`, `team_id`, `username`, `password`, `first_name`, `last_name`, `created`, `modified`, `status`) VALUES
(1, 1, 0, 'admin', 'Password1', 'Albert', 'Mananay', '2014-04-30 16:36:01', '2014-06-23 08:46:23', 0),
(2, 2, 0, 'dencio', 'Password1', 'Dennis', 'Abasa', '2014-05-06 21:09:54', '2014-05-06 21:09:54', 0),
(4, 3, 0, 'basar', 'Password1', 'Roel', 'Abasa', '2014-06-23 08:54:12', '2014-07-01 12:37:21', 0),
(5, 4, 0, 'sm', 'Password1', 'Joel', 'Hayag', '2014-07-01 12:38:48', '2014-07-02 13:25:59', 0),
(6, 5, 0, 'broker', 'Password1', 'John Paul', 'Paquibot', '2014-07-01 12:39:43', '2014-07-01 12:39:43', 0),
(7, 6, 0, 'agent007', 'Password1', 'James', 'Bond', '2014-07-01 12:40:11', '2014-07-01 12:40:11', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
