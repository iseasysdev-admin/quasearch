<?php include "header.php";?>
<?php include "nav.php";?>
<div class="container">
	<div class="row">
		<!-- start: Content -->
		<div id="content" class="col-md-6 col-md-offset-3 padding0">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Dashboard</em>
					</h3>
				</div>
				<div class="panel-body">
					<div class="col-md-12">
					<!-- for admin and first time user start here-->
					<a href="create-survey.php" class="btn btn-primary btn-whole">Create New Survey</a>
					<!-- for admin and first time user end here-->
					<!-- for admin and second time user start here-->
					
						<a href="create-survey.php" class="btn btn-primary btn-whole">Create New Survey</a>
						<a href="manage-survey.php" class="btn btn-info btn-whole">Manage Survey</a>
						<a href="publish-survey.php" class="btn btn-success btn-whole">Published Survey</a>
				
					<!-- for admin and second time user end here-->
					</div>
				</div>
			</div>
		</div>
		<!-- end: Content -->
		<!-- start: Widgets Area -->
	</div><!--/row-->
</div><!--/container-->
<div class="clearfix"></div>
<?php include "footer.php";?>